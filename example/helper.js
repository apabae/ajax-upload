$(function(){
    var row_num = 0;

    $('#addRow').on('click', function(){
        var clone = $('#clone > div').clone();

        clone.find('input')
            .attr('data-target-name', 'rownum_'+row_num)
            .attr('name', 'rownum_'+row_num)
            .prop('disabled', false);

        clone.find('a')
            .attr('data-name', 'rownum_'+row_num);

        $('#container').append(clone);

        row_num++;    
    })
}) 
