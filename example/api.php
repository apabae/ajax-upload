<?php 
// api.php

$out = array('success'=>true);

try 
{
    $parent_id = $_POST['parent_id'];

    $out['unique_name'] = $unique_name = $_POST['unique_name'] ?? uniqid('file_');

    $name = $_POST['name'];
    $path = "storage/{$parent_id}";

    if (! file_exists($path)) {
        mkdir($path);
    }

    $filename = "{$path}/{$unique_name}";
    $slice = file_get_contents($_FILES['slice']['tmp_name']);
    $f = fopen($filename, 'a');
    fwrite($f, $slice);
    fclose($f);

    // complete
    if ($_POST['pointer_end'] == $_POST['total_size'])
    {
        $out['filepath'] = $filename;
    }

}
catch (Exception $e)
{
    $out['success'] = false;
    $out['msg'] = $e->getMessage();
}

echo json_encode($out);
