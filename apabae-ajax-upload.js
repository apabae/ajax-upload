/*! apabae-ajax-upload.js v1.1 | (c) Yudha R Hidayat | yudha.id */

(function ( $ ) {

    $.fn.apabaeAjaxUpload = function(options){
        var running = false;
        var waiting = [];
        var dataName = 'submit_function';
        var dataVal = 'active';
        var formAction = this.attr('action');
        var domAttrDataName = 'data-file-id-dst';
        var domAttrDataError = 'data-upload-text-error';
        var domAttrTemporaryDisabled = 'data-temporary-disabled';
        var hasApabaeAjaxUpload = 'has_apabae_ajax_upload';

        if (this.length == 0 || this.data(dataName) == dataVal) {
            return this;
        }

        var defaults = {
            chunkSize: 1024 * 200, // 200 KB
            unique_name_response_key: 'unique_name',
            payload: {},
            error_response_key: 'msg',
            error_css_class: '',
            value_response_key: null,
            chunk_upload_url: null,
            complete : function(obj, resp) {},
            completeAll: function() {}
        }

        var settings = $.extend(
            {}, defaults, options
        );

        var complete = function(obj, status, error_text) {
            obj.next().remove();
            obj.prop('disabled', false);
            status = status === true ? true : false;

            if (status === true) {
                obj.val('');
            }

            var objError = $('span['+ domAttrDataError +'='+ obj.data('file_id') +']');
            
            if (status === false && objError.length == 0) {
                objError = $('<span></span>')
                    .addClass(settings.error_css_class)
                    .attr(domAttrDataError, obj.data('file_id'));

                obj.after(objError);
            }

            if (status === false) {
                objError.text(error_text);
            }

            return status;
        }

        var actionUpload = function(resolve, reject, obj, file, settings, formAction, i, unique_name, resp){
            var totalSize = file.size;
            var partSize  = settings.chunkSize;

            if (totalSize <= 0) {
                return;
            }
            
            i = (i == undefined ? 0 : i);

            if (i == 0) {
                var progress = $('<progress></progress>').attr('max', totalSize).val(0);
                obj.after(progress);
            }

            var pointerStart = i * partSize;
            var pointerEnd = pointerStart + partSize;

            // start
            if (pointerStart == 0) {
                $('span['+ domAttrDataError +'='+ obj.data('file_id') +']').remove();
            }

            // complete
            if (pointerStart > totalSize) {
                if (typeof options.complete == 'function') { 
                    options.complete.call(this, obj, resp);
                }

                resolve('success');

                return complete(obj, true);
            }

            if (pointerEnd > totalSize) {
                pointerEnd = totalSize;
            }

            var slice = file.slice(pointerStart, pointerEnd);
            var data = new FormData();

            $.each(settings.payload, function(k,v){
                data.append(k, v);
            })

            data.append('slice', slice);
            data.append('total_size', totalSize);
            data.append('pointer_end', pointerEnd);
            data.append('name', obj.data('name'));
            data.append('filename', obj.data('filename'));

            if (unique_name != undefined) {
                data.append('unique_name', unique_name);
            }

            $.ajax({
                cache: false,
                contentType: false,
                data: data,
                dataType: 'json',
                enctype: 'multipart/form-data',
                error: function(){
                    complete(obj);
                },
                processData: false,
                success: function(data){
                    if (data.success) {

                        var unique_name = null
                        var tmp_unique_name = data

                        // check unique_name_response_key
                        parts = settings.unique_name_response_key.split('.')

                        $.each(parts, function(k,v){
                            if (tmp_unique_name[v] == undefined) {
                                unique_name = null
                                return 
                            }
                            tmp_unique_name = tmp_unique_name[v]
                            unique_name = tmp_unique_name
                        })

                        if (! unique_name) {
                            return complete(obj);
                        }

                        obj.next().val(pointerEnd);
                        i++;
                        actionUpload(resolve, reject, obj, file, settings, formAction, i, unique_name, data);

                        if (settings.value_response_key && data[settings.value_response_key]) {
                            $('input['+ domAttrDataName +'='+ obj.data('file_id') +']').val(data[settings.value_response_key]);
                        }

                        return;
                    }

                    errorMsg = data[settings.error_response_key] == undefined ? 'Error' : data[settings.error_response_key];
                    reject(errorMsg);

                    return complete(obj, false, errorMsg);
                },
                type: 'POST',
                url: (settings.chunk_upload_url ? settings.chunk_upload_url : formAction)
            })
        }

        var runningEnd = function(){
            $('input[type=file]['+domAttrTemporaryDisabled+'=yes]').each(function(){
                $(this).prop('disabled', false);
                $(this).removeAttr(domAttrTemporaryDisabled);
            })

            running = false;
        }

        this.data(dataName, dataVal).unbind('submit').on('submit', function(){
            if (running) {
                console.log('running...');
                return false;
            }

            var file_id = 0;
            waiting = [];

            $(this).find('input[type=file]:not(:disabled)').each(function(){
                var current_file_id = file_id;
                var skip = false;

                if ($(this).data(hasApabaeAjaxUpload) == 'yes') {
                    current_file_id = $(this).data('file_id');
                    skip = true;
                }

                if (settings.value_response_key) {
                    var testObj = $('input['+domAttrDataName+'='+current_file_id+']');

                    if (testObj.length == 0) {
                        var inputHidden = $('<input/>').attr('type', 'hidden')
                            .attr(domAttrDataName, current_file_id)

                        if (settings.value_response_key != undefined) {
                            inputHidden.attr('name', $(this).attr('name'))
                        }
                        else {
                            inputHidden.data('name', $(this).attr('name'))
                        }

                        $(this).before(inputHidden);
                    }
                }

                if (! skip) {
                    $(this).data(hasApabaeAjaxUpload, 'yes');
                    $(this).data('file_id', file_id);
                    $(this).data('name', $(this).attr('name'))
                    $(this).removeAttr('name');
                }

                $(this).attr(domAttrTemporaryDisabled, 'yes');
                $(this).prop('disabled', true);

                if (this.files[0] != undefined) {
                    var file = this.files[0];
                    running = true;

                    waiting[current_file_id] = new Promise((resolve, reject) => {
                        $(this).data('filename', file.name);
                        actionUpload(resolve, reject, $(this), file, settings, formAction);
                    })
                }

                file_id++;
            })

            if (Object.keys(waiting).length > 0) {
                Promise.all(waiting).then((values) => {
                    runningEnd();

                    if (typeof options.completeAll == 'function') { 
                        options.completeAll.call(this);
                    }
                }).catch((err) => {
                    runningEnd();
                });
            }
            else {
                runningEnd();
            }

            return false;
        })

        return this;
    }

}( jQuery )); 
